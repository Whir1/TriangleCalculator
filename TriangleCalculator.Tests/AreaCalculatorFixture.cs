﻿using NUnit.Framework;

namespace TriangleCalculator.Tests
{
    [TestFixture]
    public class AreaCalculatorFixture
    {
        private AreaCalculator _target;
        private const double Epsilon = 0.001;

        [SetUp]
        public void SetUp()
        {
            //Если делать чистый тест, то надо менять на мок. Но именно в данном случае - некритично
            _target = new AreaCalculator(new PerimeterCalculator());
        }

        [Test]
        [TestCase(5, 5, 5, 10.8253)]
        public void CalculateAreaOfTriangle_ThenReturnsExpectedResult(double a, double b, double c, double expected)
        {
            var given = new Triangle(a, b, c);

            var actual = _target.Calculate(given);

            Assert.IsTrue((actual - expected) < Epsilon);
        }
    }
}