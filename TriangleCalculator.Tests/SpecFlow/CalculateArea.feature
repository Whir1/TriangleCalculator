﻿Feature: CalculateArea
	O_o

Background:
	Given CreateCalculator via factory

@Integration
Scenario Outline: Calculate area of triangle
	Given I HaveEntered length of the sides <a>, <b>, <c>
	When I Calculate
	Then Result should be equal to <expected>

Examples: | a | b | c | expected || 1 | 1 | 1 | 0.433 |
| 2 | 2 | 3 | 1.9843 |
| 7 | 5 | 3 | 6.4952 |

