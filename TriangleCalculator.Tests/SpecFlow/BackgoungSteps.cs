﻿using TechTalk.SpecFlow;

namespace TriangleCalculator.Tests.SpecFlow
{
    [Binding]
    public class BackgoungSteps
    {
        [Given("CreateCalculator via factory")]
        public void CreateCalculator()
        {
            var facory = new TriangleCalculatorFactory();
            ICalculator calculator = facory.MakeTriangleCalculator();
            ScenarioContext.Current.Set(calculator);
        }
    }
}