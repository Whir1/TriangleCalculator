﻿using TechTalk.SpecFlow;

namespace TriangleCalculator.Tests.SpecFlow
{
    [Binding]
    public class CalculateAreaSteps
    {
        private const double Epsilon = 0.001d;

        [Given("I HaveEntered length of the sides (.*), (.*), (.*)")]
        public void MakeTriangle(double a, double b, double c)
        {
            var triangle = new Triangle(a, b, c);

            ScenarioContext.Current.Set(triangle, "triangle");
        }

        [When("I Calculate")]
        public void WhenICalculatePrice()
        {
            var calculator = ScenarioContext.Current.Get<ICalculator>();
            var triangle = ScenarioContext.Current.Get<Triangle>("triangle");

            var actual = calculator.Calculate(triangle);

            ScenarioContext.Current.Set(actual, "actual");
        }

        [Then("Result should be equal to (.*)")]
        public void Assert(double expecteed)
        {
            var actual = ScenarioContext.Current.Get<double>("actual");
            NUnit.Framework.Assert.IsTrue(expecteed - actual < Epsilon);
        }

    }
}