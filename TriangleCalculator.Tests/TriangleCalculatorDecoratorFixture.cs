﻿using System;
using Moq;
using NUnit.Framework;

namespace TriangleCalculator.Tests
{
    [TestFixture]
    public class TriangleCalculatorDecoratorFixture
    {
        private TriangleCalculatorDecorator _target;
        private Mock<ICalculator> _triangleCalculatorMock;
        private Mock<ISimpleValidator<Triangle>> _validatorMock;
        private Triangle _triangle;

        [SetUp]
        public void SetUp()
        {
            _triangleCalculatorMock = new Mock<ICalculator>();
            _validatorMock = new Mock<ISimpleValidator<Triangle>>();
            _triangle = new Triangle(1, 1, 1);


            _target = new TriangleCalculatorDecorator(_triangleCalculatorMock.Object, _validatorMock.Object);
        }

        [Test]
        public void ThenTriangleIsNotValid_ThenThrows()
        {
            _validatorMock.Setup(c => c.IsValid(_triangle)).Returns(false);

            Assert.Throws<ArgumentException>(() => _target.Calculate(_triangle));

            _validatorMock.Verify(c => c.IsValid(_triangle), Times.Once);
            _triangleCalculatorMock.Verify(c => c.Calculate(It.IsAny<Triangle>()), Times.Never);
        }

        [Test]
        public void WhenCallCalculate_ThenReturnsResult()
        {
            double res = 123;
            _validatorMock.Setup(c => c.IsValid(_triangle)).Returns(true);
            _triangleCalculatorMock.Setup(c => c.Calculate(_triangle)).Returns(res);

            double act = _target.Calculate(_triangle);

            Assert.AreEqual(res, act);
            _validatorMock.Verify(c => c.IsValid(_triangle), Times.Once);
            _triangleCalculatorMock.Verify(c => c.Calculate(_triangle), Times.Once);
        }
    }
}