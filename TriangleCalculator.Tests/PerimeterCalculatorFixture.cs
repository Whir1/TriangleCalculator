﻿using NUnit.Framework;

namespace TriangleCalculator.Tests
{
    [TestFixture]
    public class PerimeterCalculatorFixture
    {
        private PerimeterCalculator _target;

        [SetUp]
        public void SetUp()
        {
            _target = new PerimeterCalculator();
        }

        [Test]
        [TestCase(1, 1, 1, 3)]
        public void WhenCalculate_ThenresultShouldBeExpected(double a, double b, double c, double expected)
        {
            var given = new Triangle(a, b, c);

            double actual = _target.Calculate(given);

            Assert.AreEqual(expected, actual);
        }
    }
}