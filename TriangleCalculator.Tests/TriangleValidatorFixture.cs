﻿using NUnit.Framework;

namespace TriangleCalculator.Tests
{
    [TestFixture]
    public class TriangleValidatorFixture
    {
        private TriangleValidator _target;

        [SetUp]
        public void SetUp()
        {
            _target = new TriangleValidator();
        }

        [Test]
        [TestCase(5, 5, 5, true)]
        [TestCase(5, 5, 10, false)]
        [TestCase(5, 10, 5, false)]
        [TestCase(10, 5, 5, false)]
        [TestCase(-10, 5, 5, false)]
        [TestCase(10, -5, 5, false)]
        [TestCase(10, 5, -5, false)]
        [TestCase(10, -5, -5, false)]
        public void WhenValidateTriangle_ThenReturnsExpectedResult(float a, float b, float c, bool expected)
        {
            var giwen = new Triangle(a, b, c);

            var actual = _target.IsValid(giwen);

            Assert.AreEqual(expected, actual);
        }
    }
}