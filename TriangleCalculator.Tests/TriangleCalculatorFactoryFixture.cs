﻿using NUnit.Framework;

namespace TriangleCalculator.Tests
{
    [TestFixture]
    public class TriangleCalculatorFactoryFixture
    {
        private TriangleCalculatorFactory _target;

        [SetUp]
        public void SetUp()
        {
            _target = new TriangleCalculatorFactory();
        }

        [Test]
        public void WhenFactoryCreateCalculator_ThenReturnsExpectedClass()
        {
            var actual = _target.MakeTriangleCalculator();

            Assert.IsNotNull(actual as TriangleCalculatorDecorator);
        }
    }
}