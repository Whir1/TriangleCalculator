﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleCalculator
{
    public class AreaCalculator : ICalculator
    {
        private readonly ICalculator _perimeterCalculator;

        public AreaCalculator(ICalculator perimeterCalculator)
        {
            _perimeterCalculator = perimeterCalculator;
        }

        public double Calculate(Triangle triangle)
        {
            var halfPerimeter = _perimeterCalculator.Calculate(triangle) / 2;

            var a = halfPerimeter - triangle.A;
            var b = halfPerimeter - triangle.B;
            var c = halfPerimeter - triangle.C;

            var area = Math.Sqrt(halfPerimeter* a * b*c);
            return area;
        }
    }
}
