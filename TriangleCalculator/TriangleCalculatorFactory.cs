﻿using System;

namespace TriangleCalculator
{
    public class TriangleCalculatorFactory
    {
        public ICalculator MakeTriangleCalculator()
        {
            var perimeterCalculator = new PerimeterCalculator();
            var areaCalculator = new AreaCalculator(perimeterCalculator);
            var validator = new TriangleValidator();

            return new TriangleCalculatorDecorator(areaCalculator, validator);
        } 
    }
}