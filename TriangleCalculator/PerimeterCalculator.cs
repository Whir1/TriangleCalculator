﻿namespace TriangleCalculator
{
    public class PerimeterCalculator : ICalculator
    {
        public double Calculate(Triangle triangle)
        {
            return triangle.A + triangle.B + triangle.C;
        }
    }
}