﻿namespace TriangleCalculator
{
    public interface ICalculator
    {
        double Calculate(Triangle triangle);
    }
}