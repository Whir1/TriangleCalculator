﻿namespace TriangleCalculator
{
    public struct Triangle
    {
        public Triangle(double a, double b, double c) : this()
        {
            A = a;
            B = b;
            C = c;
        }

        public double A { get; private set; }
        public double B { get; private set; }
        public double C { get; private set; }
    }
}