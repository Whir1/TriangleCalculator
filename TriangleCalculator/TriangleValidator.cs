﻿namespace TriangleCalculator
{
    public class TriangleValidator : ISimpleValidator<Triangle>
    {
        public bool IsValid(Triangle triangle)
        {
            if (triangle.A <= 0 || triangle.B <= 0 || triangle.C <= 0)
            {
                return false;
            }

            return (triangle.B + triangle.C > triangle.A) &&
                   (triangle.A + triangle.C > triangle.B) && 
                   (triangle.A + triangle.B > triangle.C);
        }
    }
}