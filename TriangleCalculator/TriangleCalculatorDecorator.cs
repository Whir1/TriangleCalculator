﻿using System;
using System.Runtime.CompilerServices;

namespace TriangleCalculator
{
    public class TriangleCalculatorDecorator : ICalculator
    {
        private readonly ICalculator _triangleCalculator;
        private readonly ISimpleValidator<Triangle> _validator;

        public TriangleCalculatorDecorator(ICalculator triangleCalculator, ISimpleValidator<Triangle> validator)
        {
            _triangleCalculator = triangleCalculator;
            
            _validator = validator;
        }

        /// <exception cref="ArgumentException">triangle is not valid</exception>
        public double Calculate(Triangle triangle)
        {
            if (!_validator.IsValid(triangle))
            {
                throw new ArgumentException("triangle is not valid", nameof(triangle));
            }
            return _triangleCalculator.Calculate(triangle);
        }
    }
}