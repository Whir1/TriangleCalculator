﻿namespace TriangleCalculator
{
    public interface ISimpleValidator<in T>
    {
        bool IsValid(T triangle);
    }
}